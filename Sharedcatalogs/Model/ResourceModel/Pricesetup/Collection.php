<?php
namespace IWD\Sharedcatalogs\Model\ResourceModel\Pricesetup;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
	 
    protected function _construct()
    {
        $this->_init('IWD\Sharedcatalogs\Model\Pricesetup', 'IWD\Sharedcatalogs\Model\ResourceModel\Pricesetup');
    }
	
			        
	 /// echo $this->getSelect()->__toString();die;

    
   public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
       // $countSelect->reset(\Zend_Db_Select::GROUP);
		//echo 'dddddddd'.$countSelect;die;
        return $countSelect;
    }
}
