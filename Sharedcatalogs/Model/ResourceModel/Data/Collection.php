<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD 
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Model\ResourceModel\Data;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     * @codingStandardsIgnoreStart
     */
    protected $_idFieldName = 'id';
	protected $iwd_shared_catalog ;
	protected $admin_user_table ;

    /**
     * Collection initialisation
     */
    protected function _construct()
    {
		//echo 'ffffffffffffffffffffffffffffff';die;
        // @codingStandardsIgnoreEnd
        $this->_init('IWD\Sharedcatalogs\Model\Data', 'IWD\Sharedcatalogs\Model\ResourceModel\Data');
    }
    protected function fetchjoinrecord()
   {
    $this->iwd_shared_catalog = "iwd_shared_catalog";
    $this->admin_user_table = $this->getTable("admin_user");
    $this->getSelect()
        ->join(array('adminid' =>$this->admin_user_table), $this->iwd_shared_catalog . '.ccreatedby= adminid.user_id',
        array('admin_id' => 'adminid.user_id',
            'shared_id' => $this->iwd_shared_catalog.'.id'
        )
    );
    //$this->getSelect()->where("admin_id=".$dataid);
	$this->getSelect()->__toString();
        exit();
   }
}
