<?php
namespace IWD\Sharedcatalogs\Model\Data\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    

    public function toOptionArray()
    {
         return [
            ['value' => 1, 'label' => __('Public')],
            ['value' => 0, 'label' => __('Custom')]
        ];
    }
}
