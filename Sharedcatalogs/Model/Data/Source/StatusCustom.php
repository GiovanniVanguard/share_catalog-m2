<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
  */
namespace IWD\Sharedcatalogs\Model\Data\Source;

use Magento\Framework\Data\OptionSourceInterface;

class StatusCustom implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('On')],
            ['value' => 0, 'label' => __('Off')]
        ];
    }
}
