<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
  */
namespace IWD\Sharedcatalogs\Model\Data\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Groups implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $groupOptions = $objectManager->get('\Magento\Customer\Model\Customer\Attribute\Source\Group')->getAllOptions();
          $data_array=array();
		  foreach($groupOptions as $Options)
		  {
			$data_array[]=array('value'=>$Options['value'],'label'=>$Options['label']);
			  
		  }   
		  
       //  echo '<pre>'; print_r($data_array);die;		  
            return($data_array);
    }
}
