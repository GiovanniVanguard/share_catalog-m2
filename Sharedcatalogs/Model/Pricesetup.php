<?php

namespace IWD\Sharedcatalogs\Model;

use Magento\Framework\DataObject\IdentityInterface;

class Pricesetup extends \Magento\Framework\Model\AbstractModel implements IdentityInterface
{

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'iwd_products_grid';

    /**
     * @var string
     */
    protected $_cacheTag = 'iwd_products_grid';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'iwd_products_grid';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('IWD\Sharedcatalogs\Model\ResourceModel\Pricesetup');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getProducts(\IWD\Sharedcatalogs\Model\Pricesetup $object)
    {
        $tbl = $this->getResource()->getTable(\IWD\Sharedcatalogs\Model\ResourceModel\Pricesetup::TBL_ATT_PRODUCT);
        $select = $this->getResource()->getConnection()->select()->from(
            $tbl,
            ['product_id']
        )
        ->where(
            'id = ?',
            (int)$object->getId()
        );
        return $this->getResource()->getConnection()->fetchCol($select);
    }
}
