<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Model;

use Magento\Framework\Model\AbstractModel;
use IWD\Sharedcatalogs\Api\Data\DataInterface;

class Data extends AbstractModel implements DataInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'iwd_shared_catalog';

    /**
     * Initialise resource model
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init('IWD\Sharedcatalogs\Model\ResourceModel\Data');
		
		
    }

    /**
     * Get cache identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(DataInterface::NAME);
    }

    /**
     * Set title
     *
     * @param $title
     * @return $this
     */
    public function setName($title)
    {
        return $this->setData(DataInterface::NAME, $title);
    }

    /**
     * Get data description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(DataInterface::NAME);
    }

    /**
     * Set data description
     *
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData(DataInterface::DESCRIPTION, $description);
    }

    /**
     * Get is active
     *
     * @return bool|int
     */
    public function getIsActive()
    {
        return $this->getData(DataInterface::IS_ACTIVE);
    }

    /**
     * Set is active
     *
     * @param $isActive
     * @return $this
     */
    public function setIsActive($isActive)
    {
        return $this->setData(DataInterface::IS_ACTIVE, $isActive);
    }

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(DataInterface::CREATED_AT);
    }

    /**
     * Set created by
     *
     * @param $createdby
     * @return $this
     */
    public function setCcreatedby($createdby)
    {
        return $this->setData(DataInterface::CREATEDBY, $createdby);
    }
     /**
     * Get created by
     *
     * @return string
     */
    public function getCcreatedby()
    {
        return $this->getData(DataInterface::CREATEDBY);
    }

    /**
     * Set created at
     *
     * @param $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(DataInterface::CREATED_AT, $createdAt);
    }

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(DataInterface::UPDATED_AT);
    }

    /**
     * Set updated at
     *
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(DataInterface::UPDATED_AT, $updatedAt);
    }
	
	/**
     * Get customer_group_id
     *
     * @return string
     */
    public function getCustomerGroupId()
    {
		 //echo 'ddddddddddddddd';die;
        return $this->getData(DataInterface::CUSTOMERGROUPID);
    }

    /**
     * Set customer_group_id
     *
     * @param $customer_group_id
     * @return $this
     */
    public function setCustomerGroupId($customer_group_id)
    {
        return $this->setData(DataInterface::CUSTOMERGROUPID, $customer_group_id);
    }
	/**
     * Get type
     *
     * @return string
     */
    public function gettype()
    {
        return $this->getData(DataInterface::TYPE);
    }

    /**
     * Set title
     *
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        return $this->setData(DataInterface::TYPE, $type);
    }
}
