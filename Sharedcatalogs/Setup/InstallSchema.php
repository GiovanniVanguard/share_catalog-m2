<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
  * @version    1.0.1
 */
namespace IWD\Sharedcatalogs\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('iwd_shared_catalog');

        if (!$installer->tableExists('iwd_shared_catalog')) {
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'The system-generated shared catalog ID number'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => ''],
                    'he display name of the shared catalog. Must be unique'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Describes the shared catalog'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Status'
                )
				 ->addColumn(
							'customer_group_id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							['unsigned' => true, 'nullable' => true, 'default' => null],
							'A system-generated ID. It cannot be changed'
							)
							->addColumn(
							'type',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							['unsigned' => true, 'nullable' => true, 'default' => null],
							'Indicates whether this is a custom or public shared catalog.'
							)
						   ->addColumn(
							'store_id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							255,
							[],
							'The store ID the shared catalog is assigned to	'
							)
							->addColumn(
							'tax_class_id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							[],
							'tax class id'
							)
							->addColumn(
							'ccreatedby',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							[],
							'Created by'
							)
							->addColumn(
							'ccreatedby',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							[],
							'Created by'
							)
                ->addColumn(
                    'share_product',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Shared catalog product count'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Updated At'
                );
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
