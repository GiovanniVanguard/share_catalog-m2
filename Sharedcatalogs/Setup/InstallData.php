<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @version    1.0.1
 */
namespace IWD\Sharedcatalogs\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallData implements InstallDataInterface
{

    /**
     * Install data
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $data = [
                [
                    'name' => 'Hello World!',
                    'description' => 'This is the first description.',
                    'is_active' => 1
                ],
                [
                    'name' => 'Hello Again!',
                    'description' => 'This is the second description.',
                    'is_active' => 1
                ],
                [
                    'name' => 'Welcome To The Third Title',
                    'description' => 'Here we have a slightly longer description.',
                    'is_active' => 0
                ],
                [
                    'name' => 'Fourth Coming',
                    'description' => 'This is the fourth description.',
                    'is_active' => 1
                ],
                [
                    'name' => 'TQBFJOTLD',
                    'description' => 'The quick brown fox jumped over the lazy dog.',
                    'is_active' => 0
                ]
            ];

            foreach ($data as $datum) {
                $setup->getConnection()
                    ->insertForce($setup->getTable('iwd_shared_catalog'), $datum);
            }
        }
    }
}
