<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

/**
 * Categories tree block
 */
namespace IWD\Sharedcatalogs\Block\Adminhtml\Category;

/**
 * Class Tree
 *
 * @package Magento\Catalog\Block\Adminhtml\Category
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Tree extends \Magento\Catalog\Block\Adminhtml\Category\Tree
{
    
    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
     
        return parent::_prepareLayout();
    }

     public function getCustUrl()
    {
		        $params = ['_current' => true, 'catid' => null, 'store' => null,'parent' => null];
		        return $this->getUrl('sharedcatalogs/pricesetup/index/', $params);

    }
  public function getLoadTreeUrlcust($expanded = null)
    {
        $params = ['_current' => true, 'id' => null, 'store' => null];
        if (is_null($expanded) && $this->_backendSession->getIsTreeWasExpanded() || $expanded == true) {
            $params['expand_all'] = true;
        }
        return $this->getUrl('catalog/category/categoriesJson', $params);
    }
	public function getCurrentcatid()
	{
		
	return $this->getRequest()->getParam('catid');
		
		
	}
}
