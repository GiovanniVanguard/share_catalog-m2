<?php

namespace IWD\Sharedcatalogs\Block\Adminhtml\Pricesetup\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('pricesetup_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Set Shared Catalog Price '));
    }
}
