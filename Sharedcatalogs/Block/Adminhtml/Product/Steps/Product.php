<?php
/**
 * Adminhtml block for fieldset of configurable product
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace IWD\Sharedcatalogs\Block\Adminhtml\Product\Steps;

class Product extends \Magento\ConfigurableProduct\Block\Adminhtml\Product\Steps\AttributeValues
{
    /**
     * {@inheritdoc}
     */
    public function getCaption()
    {
        return __('Product');
    }
}
