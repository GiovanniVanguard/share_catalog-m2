<?php
/**
 * Adminhtml block for fieldset of configurable product
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace IWD\Sharedcatalogs\Block\Adminhtml\Product\Steps;

class General extends \Magento\ConfigurableProduct\Block\Adminhtml\Product\Steps\SelectAttributes
{
    
	public function getCaption()
    {
        return __('General');
    }
 public function getAddNewAttributeButton($dataProvider = '')
    {
        /** @var \Magento\Backend\Block\Widget\Button $attributeCreate */
        $attributeCreate = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        );
        $attributeCreate->setDataAttribute(
            [
                'mage-init' => [
                    'productAttributes' => [
                        'dataProvider' => $dataProvider,
                        'url' => $this->getUrl('sharedcatalogs/data/edit', [
                            'store' => $this->registry->registry('current_product')->getStoreId(),
                            'product_tab' => 'variations',
                            'popup' => 1,
                            '_query' => [
                                'attribute' => [
                                    'is_global' => 1,
                                    'frontend_input' => 'select',
                                ],
                            ],
                        ]),
                    ],
                ],
            ]
        )->setType(
            'button'
        )->setLabel(
            __('Create Shared Catalog')
        );
        return $attributeCreate->toHtml();
    }

}
