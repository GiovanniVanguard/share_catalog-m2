<?php
/**
 * Adminhtml block for fieldset of configurable product
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace IWD\Sharedcatalogs\Block\Adminhtml\Product\Steps;


class Bulk extends \Magento\ConfigurableProduct\Block\Adminhtml\Product\Steps\Bulk
{
	
	public function getCaption()
    {
        return __('Price');
    }
}
