<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Block\Adminhtml\Catalog\Product\Edit\Tab;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Modalopopup extends GenericButton implements ButtonProviderInterface
{

    /**
     * Get buttong attributes
     *
     * @return array
     */
    public function getButtonData()
    {
		return  [
                    'label' => __('Next'),
                    'onclick' => 'setLocation()',
                    'class' => 'save'
                ];
    }
	
	
	protected function getButtonData()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'content' => "Shared Catalog",
                        'formElement' => 'container',
                        'componentType' => 'container',
                        'content1' => __(
                            'Apply discount for this product to Catalog.'
                        ),
                        'label' => false,
                        'template' => 'ui/form/components/complex',
                    ],
                ],
            ],
            'children' => [

                'create_productgridextend_products_button' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'formElement' => 'container',
                                'componentType' => 'container',
                                 'component' => 'Magento_Ui/js/form/components/button',
                                'actions' => [
                                    [
                                        'targetName' => $this->dataScopeName.'.configurableModal',
                                        'actionName' => 'trigger',
                                        'params' => ['active', true],
                                    ],
                                    [
                                        'targetName' => $this->dataScopeName.'.configurableModal',
                                        'actionName' => 'openModal',
                                    ],
                                ],
                                'title' => __('Price Per Catalog Configuration'),
                                'sortOrder' => 20,

                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
