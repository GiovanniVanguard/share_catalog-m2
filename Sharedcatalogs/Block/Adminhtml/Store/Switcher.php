<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace IWD\Sharedcatalogs\Block\Adminhtml\Store;

/**
 * Store switcher block
 */
class Switcher extends  \Magento\Backend\Block\Store\Switcher
{
   

    public function getajaxUrl()
    {
        return $this->getUrl('sharedcatalogs/data/wizard/', ['_current' => true]);
    }
   
}
