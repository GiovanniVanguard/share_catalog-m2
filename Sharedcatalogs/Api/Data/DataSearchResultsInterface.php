<?php
/*
 * IWD_sharedcatalogs

 * @category   IWD
 * @package    IWD_sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface DataSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get data list.
     *
     * @return \IWD\sharedcatalogs\Api\Data\DataInterface[]
     */
    public function getItems();

    /**
     * Set data list.
     *
     * @param \IWD\sharedcatalogs\Api\Data\DataInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
