<?php
/*
 * IWD_sharedcatalogs

 * @category   IWD
 * @package    IWD_sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Api\Data;

interface DataInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const DATA_ID           = 'id';
    const NAME        = 'name';
    const DESCRIPTION  = 'description';
    const IS_ACTIVE         = 'is_active';
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'updated_at';
	 const CUSTOMERGROUPID    = 'customer_group_id';
     const  TYPE  = 'type';
	 const  CREATEDBY  = 'ccreatedby';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID
     *
     * @param $id
     * @return DataInterface
     */
    public function setId($id);

	  /**
     * Get Data type
     *
     * @return string
     */
    public function getType();

	  /**
     * set Data type
     *
     * @return string
     */
    public function setType($type);

    /**
     * Get Data name
     *
     * @return string
     */
    public function getCustomerGroupId();

    /**
     * Set Data customer_group_id
     *
     * @param $customer_group_id
     * @return mixed
     */
    public function setCustomerGroupId($customer_group_id);

    /**
     * Get Data Description
     *
     * @return mixed
     */
    public function getDescription();

    /**
     * Set Data Description
     *
     * @param $description
     * @return mixed
     */
    public function setDescription($description);

    /**
     * Get is active
     *
     * @return bool|int
     */
    public function getIsActive();

    /**
     * Set is active
     *
     * @param $isActive
     * @return DataInterface
     */
    public function setIsActive($isActive);

    /**
     * Get created by
     *
     * @return string
     */
    public function getCcreatedby();

    /**
     * set created by
     *
     * @param $createdby
     * @return DataInterface
     */
    public function setCcreatedby($createdby);

    /**
     * Get updated at
     *
     * @return string
     */
	  /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * set created at
     *
     * @param $createdAt
     * @return DataInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * set updated at
     *
     * @param $updatedAt
     * @return DataInterface
     */
    public function setUpdatedAt($updatedAt);
}
