<?php 
namespace IWD\Sharedcatalogs\Ui\Component\DataProvider\SearchResult;

class Items extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{ 
     protected function _initSelect()
    {
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      parent::_initSelect(); 	
     $productNameAttributeId=$objectManager->create('Magento\Eav\Model\Config')->getAttribute(\Magento\Catalog\Model\Product::ENTITY, \Magento\Catalog\Api\Data\ProductInterface::NAME)->getAttributeId();
     $this->getSelect()->join(['product_varchar'=>$this->getTable('catalog_product_entity_varchar')],"product_varchar.entity_id = main_table.entity_id AND product_varchar.attribute_id=$productNameAttributeId",[])->columns(['product_name'=>'product_varchar.value']);
     $productPricAttributeId=$objectManager->create('Magento\Eav\Model\Config')->getAttribute(\Magento\Catalog\Model\Product::ENTITY, \Magento\Catalog\Api\Data\ProductInterface::PRICE)->getAttributeId();
     $this->getSelect()->join(['product_decimal'=>$this->getTable('catalog_product_entity_decimal')],"product_decimal.entity_id = main_table.entity_id AND product_decimal.attribute_id=$productPricAttributeId",[])->columns(['price'=>'product_decimal.value']);
     $this->getSelect()->join(['qtys'=>$this->getTable('cataloginventory_stock_item')],"qtys.product_id = main_table.entity_id AND qtys.stock_id=1",[])->columns(['qty'=>'qtys.qty']);
	 $producttaxAttributeId=$objectManager->create('Magento\Eav\Model\Config')->getAttribute(\Magento\Catalog\Model\Product::ENTITY,'tax_class_id')->getAttributeId();
     $this->getSelect()->join(['product_tax'=>$this->getTable('catalog_product_entity_int')],"product_tax.entity_id = main_table.entity_id AND product_tax.attribute_id=$producttaxAttributeId",[])->columns(['tax'=>'product_tax.value']);
     $productVisAttributeId=$objectManager->create('Magento\Eav\Model\Config')->getAttribute(\Magento\Catalog\Model\Product::ENTITY, \Magento\Catalog\Api\Data\ProductInterface::VISIBILITY)->getAttributeId();
  	 $this->getSelect()->join(['product_vis'=>$this->getTable('catalog_product_entity_int')],"product_vis.entity_id = main_table.entity_id AND product_vis.attribute_id=$productVisAttributeId",[])->columns(['value'=>'product_vis.value']);
      	$this->addMyCustomFilter();   
		//echo $this->getSelect()->__toString();die;
		return $this;

}
    public function addMyCustomFilter(){
		   $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
           $request=$objectManager->get('Magento\Framework\App\RequestInterface');
		    $catid=(int)$request->getParam('catid',0);  		     
		   if($catid>0){
		    $this->getSelect()->join(['product_cat'=>$this->getTable('catalog_category_product')],"product_cat.product_id = main_table.entity_id and product_cat.category_id=$catid",[])->columns(['catid'=>'product_cat.category_id']);
		   }

        return $this;
    }
	public function addFieldToFilter($field, $condition = null)
    {
		//echo $field;die;
        if ($field == 'main_table.product_name') {
            $field = 'product_varchar.value';
            if (is_array($condition) && isset($condition['eq'])) {
                $condition = $condition['eq'];
            }
        }
		//echo $field; die;
		if ($field == 'main_table.value') {
            $field = 'product_vis.value';
            if (is_array($condition) && isset($condition['eq'])) {
                $condition = $condition['eq'];
            }
        }
			//echo $field; die;
		if ($field == 'value') {
            $field = 'product_vis.value';
            if (is_array($condition) && isset($condition['eq'])) {
                $condition = $condition['eq'];
            }
        }
		if ($field == 'main_table.tax') {
            $field = 'product_tax.value';
            if (is_array($condition) && isset($condition['eq'])) {
                $condition = $condition['eq'];
            }
        }
		if ($field == 'tax') {
            $field = 'product_tax.value';
            if (is_array($condition) && isset($condition['eq'])) {
                $condition = $condition['eq'];
            }
        }
		if ($field == 'main_table.price') {
            $field = 'product_decimal.value';
            if (is_array($condition) && isset($condition['eq'])) {
                $condition = $condition['eq'];
            }
        }
		if ($field == 'main_table.qty') {
            $field = 'qtys.qty';
            if (is_array($condition) && isset($condition['eq'])) {
                $condition = $condition['eq'];
            }
        }
		if ($field == 'price') {
            $field = 'product_decimal.value';
            if (is_array($condition) && isset($condition['eq'])) {
                $condition = $condition['eq'];
            }
        }
        return parent::addFieldToFilter($field, $condition);
    }

}