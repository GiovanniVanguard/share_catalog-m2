<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace IWD\Sharedcatalogs\Ui\Component\DataProvider;

/**
 * Class DataProvider
 */
class CustomProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{
     protected function prepareUpdateUrl()    {

		 parent::prepareUpdateUrl();    
		  $catid=(int)$this->request->getParam('catid',0);
		  if($catid>0){
		  $this->data['config']['update_url']=$this->data['config']['update_url'].'catid/'.$catid.'/';
		  }
         //echo "<pre>"; print_r($this->data['config']);die;
        if (!isset($this->data['config']['filter_url_params'])) {
            return;
        }
        foreach ($this->data['config']['filter_url_params'] as $paramName => $paramValue) {
            if ('*' == $paramValue) {
                $paramValue = $this->request->getParam($paramName);
            }
            if ($paramValue) {
                $this->data['config']['update_url'] = sprintf(
                    '%s%s/%s',
                    $this->data['config']['update_url'],
                    $paramName,
                    $paramValue
                );
                $this->addFilter(
                    $this->filterBuilder->setField($paramName)->setValue($paramValue)->setConditionType('eq')->create()
                );
            }
        }
    }

}
