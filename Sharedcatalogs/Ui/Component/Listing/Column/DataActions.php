<?php
/*
 * IWD_Sharedcatalogs
 */
namespace IWD\Sharedcatalogs\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class DataActions extends Column
{
    const URL_PATH_EDIT = 'sharedcatalogs/data/edit';
    const URL_PATH_DELETE = 'sharedcatalogs/data/delete';
	const URL_PATH_COMP = 'b2b/company';
    const URL_PATH_SETPRICE= 'sharedcatalogs/pricesetup/index/';


    /**
     * URL builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
	 protected $userFactory;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
	   \Magento\User\Model\UserFactory $userFactory,

        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
		$this->userFactory = $userFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
		
		 
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['id'])) {
					if($item['ccreatedby']!=''){
					$item['ccreatedby']=$this->getRoleData($item['ccreatedby']);
					}
					$item['share_product']=$this->getProductcount($item['id']);					
                    $item[$this->getData('name')] = [
                        'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('Edit')
                        ],
						  'setprice' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_SETPRICE,
                                [
                                    'shared_cat_id' => $item['id']
                                ]
                            ),
                            'label' => __('Set pricing and structure')
                        ],
						'assigncompany' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_COMP,
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('Assign companies')
                        ],
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete "${ $.$data.name }"'),
                                'message' => __('Are you sure you want to delete the Data: "${ $.$data.name }"?')
                            ]
                        ]
                    ];
                }
            }
        }
        return $dataSource;
    }
	public function getProductcount($id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$customcollection= $objectManager->get('IWD\Pricesetup\Model\Customcollection')->getCollection()			                   
					         ->addFieldToFilter('share_cat_id',$id)->getData();
		
		return count($customcollection);
	}
	public function getRoleData($userId)
{
    $user = $this->userFactory->create()->load($userId);
    //$role = $user->getRole();
    $data = $user->getData();
    return $data['username'];
}
}
