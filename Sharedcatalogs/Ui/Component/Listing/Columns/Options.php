<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace IWD\Sharedcatalogs\Ui\Component\Listing\Columns;

use Magento\Framework\Data\OptionSourceInterface;

class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    
 const NAME = 'VISIBILITY';

    public function toOptionArray()
    {
		//echo '<pre>';print_r($this->getExistingOptions());die;
         return $this->getExistingOptions();
    }
function getExistingOptions() {
	$object_Manager = \Magento\Framework\App\ObjectManager::getInstance();
	$eavConfig = $object_Manager->get('\Magento\Eav\Model\Config');
	$attribute = $eavConfig->getAttribute('catalog_product', 'visibility');
	$options = $attribute->getSource()->getAllOptions();

	$optionsExists = array();

	foreach($options as $option) {
		$optionsExists[] = array('value' =>$option['value'], 'label' =>$option['label']);
	}

	return $optionsExists;
	}
}


