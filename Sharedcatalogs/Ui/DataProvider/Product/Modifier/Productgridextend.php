<?php
namespace IWD\Sharedcatalogs\Ui\DataProvider\Product\Modifier;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Api\GroupRepositoryInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Ui\Component\Form;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\ArrayManager;
use IWD\Sharedcatalogs\Model\DataRepository;
class Productgridextend extends AbstractModifier
{
    const SORT_ORDER = 40;

    protected $locator;

    protected $websiteRepository;

    protected $groupRepository;

    protected $storeRepository;

    protected $websitesOptionsList;

    protected $storeManager;

    protected $websitesList;

    private $dataScopeName;
    
    protected $urlBuilder;
   protected $dataRepository;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;
    public function __construct(
        LocatorInterface $locator,
        StoreManagerInterface $storeManager,
        WebsiteRepositoryInterface $websiteRepository,
        GroupRepositoryInterface $groupRepository,
        StoreRepositoryInterface $storeRepository,
        DataRepository $DataRepository,
	    UrlInterface $urlBuilder,
        ArrayManager $arrayManager,
	    \Magento\Framework\ObjectManagerInterface $objectManager,

        $dataScopeName
    ) {
        $this->locator = $locator;
        $this->storeManager = $storeManager;
        $this->websiteRepository = $websiteRepository;
        $this->groupRepository = $groupRepository;
        $this->storeRepository = $storeRepository;
        $this->dataScopeName = $dataScopeName;
	    $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
        $this->dataRepository = $DataRepository;
      	$this->_objectManager = $objectManager;
    }
public function modifyData(array $data)
    {
 $productId = $this->locator->getProduct()->getId();

 $customcollection= $this->_objectManager->get('IWD\Pricesetup\Model\Customcollection')->getCollection();
     $allshared=$customcollection->addFieldToFilter('entity_id',$productId)->getData();
    $existing=array();
     foreach($allshared as $sharedexist)
     {
      
    $existing[]=(string)$sharedexist['share_cat_id'];  
      
    } 
 if ($productId != null ) {
 $data = array_replace_recursive(
 $data,
 [
 $productId => ['product' =>['shared_ids' => $existing] ]
 ]
 );
 }
  return $data;
 }

    public function modifyMeta(array $meta)
    {
        if (!$this->storeManager->isSingleStoreMode()) {
            $meta = array_replace_recursive(
                $meta,
                [
                    'productgridextend' => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'additionalClasses' => 'admin__fieldset-product-productgridextend',
                                    'label' => __('Product in shared catalog'),
                                    'collapsible' => true,
									'dataScope' => 'data.product',
                                    'componentType' => Form\Fieldset::NAME,
                                    'sortOrder' => $this->getNextGroupSortOrder(
                                        $meta,
                                        'search-engine-optimization',
                                        self::SORT_ORDER
                                    ),
                                ],
                            ],
                        ],
                        'children' => $this->getPanelChildren(),						
                    ],
                ]
            );
        }

        return $meta;
    }

    protected function getPanelChildren()
    {
        return [
            'productgridextend_products_button_set' => $this->getButtonSet()

        ];
    }

    protected function getButtonSet()
    {
		        $fieldCode = 'shared_ids';

        return [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Shared Catalog'),
                            'breakLine' => false,
                            'visible' => true,
							'dataType' => 'text',
							'formElement' => 'container',
                            'componentType' => 'container',
                            'component' => 'Magento_Ui/js/form/components/group',
							'scopeLabel' => __('[GLOBAL]'),


                        ],
                    ],
                ],
                'children' => [
                    $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'formElement' => 'select',
                                    'componentType' => 'field',
                                    'component' => 'IWD_Sharedcatalogs/js/components/new-shared',
                                    'filterOptions' => true,
                                    'chipsEnabled' => true,
                                    'disableLabel' => true,
									'multiple'=>true,
                                    'levelsVisibility' => '1',									
                                    'elementTmpl' => 'ui/grid/filters/elements/ui-select',
                                    'options' => $this->getSharedList(),
                                    'listens' => [
                                        'index=create_shared:responseData' => 'setParsed',
                                        'newOption' => 'toggleOptionSelected'
                                    ],
                                    'config' => [
                                        'dataScope' => $fieldCode,
                                        'sortOrder' => 10,
                                    ],
                                ],
                            ],
                        ],
                    ],
                          ]
            ];

    }
	  protected function getSharedList()		  
		{       
		
		          $categoryById=array();
                   $collection = $this->dataRepository->getAlldata();
				   foreach($collection as $data)
				   {
					//if($data['is_active']==1) {
    				$categoryById[] =array('value' => $data['id'],'is_active' => $data['is_active'],'label' => $data['name']);	

				//	}					  
				  }
				  return $categoryById;
		}		
		

}