<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Controller\Adminhtml\Data;

use IWD\Sharedcatalogs\Controller\Adminhtml\Data;

class Index extends Data
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
       public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('IWD_Sharedcatalogs::data/index');
        $resultPage->addBreadcrumb(__('IWD'), __('IWD'));
        $resultPage->addBreadcrumb(__('Manage Shared Catalog'), __('Shared Catalog'));
        $resultPage->getConfig()->getTitle()->prepend(__('Shared Catalog'));

        return $resultPage;
    }
}
