<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @license    https://github.com/IWD/magento2-sharedcatalogs-uicomponent/blob/master/LICENSE.md
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Controller\Adminhtml\Data;

use IWD\Sharedcatalogs\Controller\Adminhtml\Data;

class Add extends Data
{
    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
