<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Controller\Adminhtml\Data;

use IWD\Sharedcatalogs\Controller\Adminhtml\Data;

class Edit extends Data
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $dataId = $this->getRequest()->getParam('id');
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('IWD_Sharedcatalogs::data')
            ->addBreadcrumb(__('Catalog'), __('Catalog'))
            ->addBreadcrumb(__('Manage Catalog'), __('Manage Catalog'));

        if ($dataId === null) {
            $resultPage->addBreadcrumb(__('New Catalog'), __('New Catalog'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Catalog'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Catalog'), __('Edit Catalog'));
            $resultPage->getConfig()->getTitle()->prepend(__('Catalog Name - ').
                $this->dataRepository->getById($dataId)->getName()
            );
        }
        return $resultPage;
    }
}
