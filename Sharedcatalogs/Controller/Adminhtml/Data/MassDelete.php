<?php
/*
 * IWD_Sharedcatalogs

 * @category   IWD
 * @package    IWD_Sharedcatalogs
 * @copyright  Copyright (c) 2017 IWD
 * @license    https://github.com/IWD/magento2-sharedcatalogs-uicomponent/blob/master/LICENSE.md
 * @version    1.0.0
 */
namespace IWD\Sharedcatalogs\Controller\Adminhtml\Data;

use IWD\Sharedcatalogs\Model\Data;

class MassDelete extends MassAction
{
    /**
     * @param Data $data
     * @return $this
     */
    protected function massAction(Data $data)
    {
        $this->dataRepository->delete($data);
        return $this;
    }
}
