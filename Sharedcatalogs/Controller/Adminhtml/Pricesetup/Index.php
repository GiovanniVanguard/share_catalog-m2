<?php

namespace IWD\Sharedcatalogs\Controller\Adminhtml\Pricesetup;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use IWD\Sharedcatalogs\Api\DataRepositoryInterface;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    
	 protected $customerSession;
     protected $dataRepository;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
	    DataRepositoryInterface $dataRepository,
		\Magento\Customer\Model\Session $customerSession,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
		$this->customerSession = $customerSession;
		$this->dataRepository=$dataRepository;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
		$shared_cat_id = (int)$this->getRequest()->getParam('shared_cat_id',0);
		
        $resultPage->setActiveMenu('IWD_Sharedcatalogs::pricesetup');
        $resultPage->getConfig()->getTitle()->prepend(__($this->dataRepository->getById($shared_cat_id)->getName().' Assign Product'));
         $catid = (int)$this->getRequest()->getParam('catid', 0);
         $this->customerSession->setSharedCatId($shared_cat_id);
         $this->customerSession->setCatId($catid);
		 
         $store=(int)$this->getRequest()->getParam('store', 0);
         //$session = $this->_objectManager->get('Magento\Customer\Model\Session');
         $this->customerSession->setTestKey($store);
        return $resultPage;
    }
	

    /**
     * Is the user allowed to view the attachment grid.
     *
     * @return bool
     */
    /*protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('IWD_Sharedcatalogs::pricesetup');
    }*/
}
