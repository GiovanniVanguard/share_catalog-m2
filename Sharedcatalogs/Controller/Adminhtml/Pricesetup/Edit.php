<?php

namespace IWD\Sharedcatalogs\Controller\Adminhtml\Pricesetup;

use Magento\Backend\App\Action;
use IWD\Sharedcatalogs\Api\DataRepositoryInterface;

class Edit extends \Magento\Backend\App\Action
{

    /**
     * Data repository
     *
     * @var DataRepositoryInterface
     */
    protected $dataRepository;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
       

	   protected $objectManager;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
	    \Magento\Framework\ObjectManagerInterface $objectManager,
	     DataRepositoryInterface $dataRepository,
        \Magento\Framework\Registry $registry

    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
		$this->dataRepository= $dataRepository;
		$this->objectManager = $objectManager;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    /*protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('iwd_pricesetup::attachment_save');
    }*/

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    /**
     * Edit Pricesetup
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $dataId = $this->getRequest()->getParam('shared_cat_id');		
		//echo 'ddddddddddd';die;
        $model = $this->_objectManager->create('IWD\Sharedcatalogs\Model\Pricesetup');
          $resultPage = $this->resultPageFactory->create(); 
		          $resultPage->setActiveMenu('IWD_Sharedcatalogs::pricesetup')
            ->addBreadcrumb(__('Data'), __('Data'))
            ->addBreadcrumb(__('Manage Data'), __('Manage Data'));
		  if ($dataId === null) {
            $resultPage->addBreadcrumb(__('New Data'), __('New Data1'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Data1'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Data'), __('Edit Data'));
            $resultPage->getConfig()->getTitle()->prepend($this->dataRepository->getById($dataId)->getName());

        }

        return $resultPage; 
		}
        }
