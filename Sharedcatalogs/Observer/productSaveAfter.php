<?php
namespace IWD\Sharedcatalogs\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductSaveAfter implements ObserverInterface
{
public function __construct(
    \Magento\Framework\App\RequestInterface $request,
	\Magento\Framework\ObjectManagerInterface $objectManager
)
{    
    $this->_request = $request;
	$this->_objectManager = $objectManager;
}
 public function execute(\Magento\Framework\Event\Observer $observer)

 {
        $post= $this->_request->getPost();
        $_product = $observer->getProduct();
        $productId=$_product->getEntityId();  
        $customcollection= $this->_objectManager->get('IWD\Pricesetup\Model\Customcollection')->getCollection();
        $allshared=$customcollection->addFieldToFilter('entity_id',$productId)->getData();
		$existing=array();
		 foreach($allshared as $sharedexist)
		 {
		  
	    $existing[$sharedexist['id']]=$sharedexist['share_cat_id'];  
		  
		 }		
		 //echo '<pre>'; print_r($post['product']['shared_ids']);
		foreach($post['product']['shared_ids'] as $shared){
			 $customcollection= $this->_objectManager->get('IWD\Pricesetup\Model\Customcollection')->getCollection();
			 $checkdata=$customcollection->addFieldToFilter('share_cat_id',$shared)->addFieldToFilter('entity_id',$productId)
			                              ->getData();

			$data=$this->_objectManager->create('IWD\Pricesetup\Model\Customcollection');
				if (($key = array_search($shared,$existing))!== false) {
				unset($existing[$key]);
				} 
			if(count($checkdata)==0){
             				
				$data->setData('entity_id', $productId);
				$data->setData('store_id', 0);						
				$data->setData('share_cat_id',$shared);
				$data->setData('share_price', 0.00);
				$data->setData('offer', 1);	
                $data->save();	
				unset($data);
			  }		
			  }
			foreach($existing as $key=>$val)
			{
			try {
			$data =$this->_objectManager->create('IWD\Pricesetup\Model\Customcollection');
			$data->load($key);
			$data->delete();
			} catch (\Exception $e) {
			$this->messageManager->addError($e->getMessage());
			}  

			}
 }

}