<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 ***************************************************************************/

namespace IWD\Pricesetup\Controller\Adminhtml\Manage;

class NewAction extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $session
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->session = $session;
        parent::__construct($context);
    }
    public function execute()
    {
        
        /* Get store value from url and save in session then get in save.php*/
        $store=(int)$this->session->getTestKey();
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('IWD_Pricesetup::grid');
        $resultPage->getConfig()->getTitle()->prepend(__('Custom price set up to products'));
        return $resultPage;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('IWD_Pricesetup::pricesetupproduct');
    }
}
