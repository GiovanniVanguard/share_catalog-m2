<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 ***************************************************************************/
namespace  IWD\Pricesetup\Controller\Adminhtml\Manage;

class Grid extends \Magento\Backend\App\Action
{
    public function execute()
    {
        
            $this->getResponse()->setBody(
                $this->_view->getLayout()->
                createBlock('IWD\Pricesetup\Block\Adminhtml\Product\Grid')->toHtml()
            );
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('IWD_Pricesetup::pricesetupproduct');
    }
}
