<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Copyright		: Copyright (c) 2018 iwd, http://www.iwd.com
 Support Email	: support@iwd.com 
 ***************************************************************************/
namespace  IWD\Pricesetup\Controller\Adminhtml\Manage;

class Productinfo extends \Magento\Backend\App\Action
{
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
    ) {
        parent::__construct($context);
        $this->resultLayoutFactory = $resultLayoutFactory;
    }
    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('pricesetup.product.edit.tab.productinfo')
            ->setProductsPricesetup($this->getRequest()->getPost('products_pricesetup', null));
        return $resultLayout;
    }
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('IWD_Pricesetup::pricesetupproduct');
    }
}
