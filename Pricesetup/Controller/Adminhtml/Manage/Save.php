<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 ***************************************************************************/
namespace IWD\Pricesetup\Controller\Adminhtml\Manage;

use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{
    protected $_collection;
    protected $customerSession;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection,
		\Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
    
        $this->_collection = $collection;
	   $this->customerSession = $customerSession;

        parent::__construct($context);
    }
    
    public function execute()
    {
        /*Store value get from session whish is saved in newaction.php*/
        $storeId=$this->customerSession->getTestKey();
        $jsHelper = $this->_objectManager->get('Magento\Backend\Helper\Js');
        $post=$this->getRequest()->getPost()->toarray();		
	//	echo '<pre>';print_r($post);die;
		$sharedid=(int)$this->customerSession->getSharedCatId();
		$store=(int)$this->customerSession->getTestKey();
		if($sharedid<1)$this->_redirect('*/*/new');
        $entityIds=$post['share_price'];//$jsHelper->decodeGridSerializedInput($post['links']['pricesetup']);
		unset($entityIds[0]);

		  //  echo "<pre>"; print_r($entityIds);die;
        if (!is_array($entityIds) || empty($entityIds)) {
            $this->messageManager->addError(__('Please select product(s).'));
            $this->_redirect('*/*/new');
        } else {
            /**display the selected product and save it in database in custom_product_collection table **/
            $records=$this->_collection->getData();            
            $all_store=$this->_objectManager->create('\Magento\Store\Api\StoreRepositoryInterface')->getList();   
                 			
          			foreach($entityIds as $pid=>$key){
						  $customcollection= $this->_objectManager->get('IWD\Pricesetup\Model\Customcollection')->getCollection()		                ->addFieldToFilter('share_cat_id',$sharedid);
					    $data=$this->_objectManager->create('IWD\Pricesetup\Model\Customcollection');
						$checkdata=$customcollection->addFieldToFilter('entity_id',$pid)->getData();
						$customp=$post['share_price'][$pid];
						$offer=$post['offer'][$pid]; 
						//echo $customcollection->getSelect()->__toString();
						if($customp!=''){
						if(count($checkdata)>0){						                      						
						$data->setData('id', $checkdata[0]['id']);	
						}else{							
						$data->setData('entity_id', $pid);
                        $data->setData('store_id', $store);						
					    $data->setData('share_cat_id',$this->customerSession->getSharedCatId());
                        $data->setData('share_price', $customp);
						$data->setData('offer', $offer);
						}
						$data->save();
						}
						unset($data);
						unset($customcollection);
                            }	///	die;
			      $this->messageManager->addSuccess(__('Custom price added')); 
            $this->_redirect('sharedcatalogs/data/index/');
        }
    }
    protected function _isAllowed()
    {
        return true;
    }
}
