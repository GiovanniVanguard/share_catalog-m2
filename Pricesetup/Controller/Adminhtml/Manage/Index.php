<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 

 ***************************************************************************/
namespace IWD\Pricesetup\Controller\Adminhtml\Manage;
use IWD\Sharedcatalogs\Api\DataRepositoryInterface;

class Index extends \Magento\Backend\App\Action
{
	
    /**
     * Data repository
     *
     * @var DataRepositoryInterface
     */
	 protected $customerSession;
    protected $dataRepository;
    protected $resultPageFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
	    DataRepositoryInterface $dataRepository,
		\Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
    
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
	     $this->customerSession = $customerSession;
		$this->dataRepository=$dataRepository;

    }
    public function execute()
    {
        
        $resultPage = $this->resultPageFactory->create();     
		$shared_cat_id = $this->getRequest()->getParam('shared_cat_id');
		
        $resultPage->setActiveMenu('IWD_Pricesetup::grid');
        $resultPage->getConfig()->getTitle()->prepend(__($this->dataRepository->getById($shared_cat_id)->getName().'Assign Product'));
         $catid = (int)$this->getRequest()->getParam('catid', 0);
         $this->customerSession->setSharedCatId($shared_cat_id);
         $this->customerSession->setCatId($catid);
        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }
         $store=(int)$this->getRequest()->getParam('store', 0);
         //$session = $this->_objectManager->get('Magento\Customer\Model\Session');
         $this->customerSession->setTestKey($store);
         return $resultPage;
    }
    
    protected function _isAllowed()
    {
        return true;
    }
}
