<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 ***************************************************************************/
namespace IWD\Pricesetup\Block\Adminhtml;

class Render extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{
    /**
     * Render product name to add Configure link
     *
     * @param   \Magento\Framework\DataObject $row
     * @return  string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$currencysymbol = $objectManager->create('Magento\Directory\Model\Currency')->getCurrencySymbol();

        $rendered = parent::render($row);
        $isConfigurable = $row->canConfigure();
		$flag2='';		
		$flag='';
		if($row->getOffer()==1){ $flag="selected";}
	    if($row->getOffer()==2){ $flag2="selected";}
        //  number_format($rendered,2)
        $style = $isConfigurable ? '' : 'disabled';
        $prodAttributes = $isConfigurable ? sprintf(
            'list_type = "product_to_add" product_id = %s',
            $row->getId()
        ) : 'disabled="disabled"';
		return '<select name="offer['.$row->getId().']" id="main_section_filter_in_products_offer_'.$row->getId().'" data-ui-id="widget-grid-column-filter-checkbox-0-filter-in-products" class="no-changes admin__control-select">
<option '.$flag.' value="1">Fixed</option>
<option '.$flag2.' value="2">Percentage</option>
</select><span data-bind="text: addbefore">'.$currencysymbol.'</span>
            <input  type="text" name="share_price['.$row->getId().']" value="'.number_format($rendered,2).'" class="input-text admin__control-text no-changes " style="max-width:130px;">';
   
    }
} 
   

