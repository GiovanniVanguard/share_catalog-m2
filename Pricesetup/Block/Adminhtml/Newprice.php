<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 ***************************************************************************/
namespace IWD\Pricesetup\Block\Adminhtml;

class Newprice extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{
    /**
     * Render product name to add Configure link
     *
     * @param   \Magento\Framework\DataObject $row
     * @return  string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$currencysymbol = $objectManager->create('Magento\Directory\Model\Currency')->getCurrencySymbol();

        $rendered = parent::render($row);
		$price='';
		if($row->getOffer()==1){
		  $price=number_format($rendered-abs($row->getSharePrice()),2);
		}else{			
		 $price=number_format($rendered-abs($row->getId()*$row->getSharePrice()*0.01),2);
		}
         return $currencysymbol.$price;

   
    }
} 
   

