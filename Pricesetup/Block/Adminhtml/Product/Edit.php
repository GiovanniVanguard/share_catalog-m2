<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 ***************************************************************************/
namespace IWD\Pricesetup\Block\Adminhtml\Product;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_product';
        $this->_blockGroup = 'IWD_Pricesetup';
        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Products'));
        $this->buttonList->update('delete', 'label', __('Delete Block'));
		 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
   $customerSession = $objectManager->create('Magento\Customer\Model\Session');
         $uri='';
		$sharedid=(int)$customerSession->getSharedCatId();
		if($sharedid>0) $uri='shared_cat_id/'.$sharedid;
        $uriid=(int)$customerSession->getCatId();
         if($uriid)$uri=$uri.'/catid/'.$uriid;
		// $this->buttonList->update('back', 'label', __('Block'));
              $this->addButton(
        'back',
        [
            'label' => __('Back'),
            'onclick' => "setLocation('" . $this->getUrl('sharedcatalogs/pricesetup/index/'.$uri) . "')",
            'class' => 'back'
        ],
        -1
    );
    }
}
