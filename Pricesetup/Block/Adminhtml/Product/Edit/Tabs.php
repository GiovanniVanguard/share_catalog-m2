<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 ***************************************************************************/
namespace IWD\Pricesetup\Block\Adminhtml\Product\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('product_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Add price with share catalog'));
    }
    protected function _prepareLayout()
    {
          $this->addTab(
              'pricesetup',
              [
                  'label' => __('Add price with share catalog'),
                  'url' => $this->getUrl('pricesetup/manage/productinfo', ['_current' => true]),
                  'class' => 'ajax'
              ]
          );
          return parent::_prepareLayout();
    }
}
