<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 

 ***************************************************************************/
namespace  IWD\Pricesetup\Block\Adminhtml\Product\Edit\Tab;

use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;

class Productinfo extends Extended
{
    protected $_coreRegistry = null;
    protected $_linkFactory;
    protected $_setsFactory;
    protected $_productFactory;
    protected $_type;
    protected $_status;
    protected $_visibility;
    protected $_customcollection;
    protected $moduleManager;
    protected $customerSession;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Module\Manager $moduleManager,
        \IWD\Pricesetup\Model\Customcollection $customcollection,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\Product\LinkFactory $linkFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setsFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\Product\Type $type,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $status,
        \Magento\Catalog\Model\Product\Visibility $visibility,
		\Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_linkFactory = $linkFactory;
        $this->_customcollection = $customcollection;
        $this->_setsFactory = $setsFactory;
        $this->_productFactory = $productFactory;
	    $this->customerSession = $customerSession;
        $this->_type = $type;
        $this->_status = $status;
        $this->_visibility = $visibility;
        $this->_coreRegistry = $coreRegistry;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }
 
    protected function _construct()
    {
        parent::_construct();
        $this->setId('main_section');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }
    
	protected function _getSelectedProducts()
	{
	$sharedid=$this->customerSession->getSharedCatId();

	$ids=$this->_customcollection->getCollection()->addFieldToFilter('share_cat_id', $sharedid)->getData();
	 $entity_id=array();
      foreach($ids as $id)
	  {
      $entity_id[]=$id['entity_id'];
      }	  
	  return $entity_id;
	} protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $productIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
	
    protected function _prepareCollection()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
		    $sharedid=$this->customerSession->getSharedCatId();
			$custcatid = (int)$this->getRequest()->getParam('catid', 0);
			if($custcatid>0){
			           $catid=$custcatid;
	
			}else{
			           $catid=$this->customerSession->getCatId();
	
			}

        $collection = $this->_productFactory->create()->getCollection()->addAttributeToSelect(
            'sku'
        )->addAttributeToSelect(
            'name'
        )->addAttributeToSelect(
            'attribute_set_id'
        )->addAttributeToSelect(
            'type_id'
        );
      
            $collection->joinAttribute(
                'name',
                'catalog_product/name',
                'entity_id'
            );
            $collection->joinAttribute(
                'custom_name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner'
            );
            $collection->joinAttribute(
                'status',
                'catalog_product/status',
                'entity_id',
                null,
                'inner'
            );
            $collection->joinAttribute(
                'visibility',
                'catalog_product/visibility',
                'entity_id',
                null,
                'inner'
            );
				if($catid>0){
		    $collection->addCategoriesFilter(['eq' =>$catid]);
			}
			
        $collection->joinAttribute('price', 'catalog_product/price', 'entity_id', null, 'left', $storeId);
        $collection->addAttributeToSelect('price');
	 $collection->getSelect()->joinLeft(['share_product'=>$collection->getTable('iwd_pricesetup_products')],
                'e.entity_id = share_product.entity_id and share_product.share_cat_id='.$sharedid,
                ['share_price'=>'share_product.share_price','offer'=>'share_product.offer']);        
       //$collection->getSelect()->group('e.entity_id');
        $this->setCollection($collection);
        return parent::_prepareCollection();
		}
 
    protected function _prepareColumns()
    {
              
            $this->addColumn(
                'in_products',
                [
				   'header' => __('ID'),
                    'name' => 'in_products',
                    'align' => 'center',
                    'index' => 'entity_id',
                ]
            );
            
        $this->addColumn(
            'name',
            [
                        'header' => __('Name'),
                        'index' => 'name'
                ]
        );
        
 
        $this->addColumn(
            'sku',
            [
                        'header' => __('SKU'),
                        'index' => 'sku'
                ]
        );
 
        $this->addColumn(
            'price',
            [
                        'header' => __('Price'),
                        'type' => 'currency',
                        'currency_code' => (string)$this->_scopeConfig->getValue(
                            \Magento\Directory\Model\Currency::XML_PATH_CURRENCY_BASE,
                            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                        ),
						'index' => 'price',
                        'header_css_class' => 'col-price price',
                        'column_css_class' => 'col-price price'
                ]
        );
		
		 $this->addColumn(
            'share_price[]',
            [
                        'header' => __('Share Price'),
                        'type' => 'text',  
                        'renderer' =>'IWD\Pricesetup\Block\Adminhtml\Render',					
                        'index' => 'share_price',
						'column_css_class'=>'col-sharedprice',
						'header_css_class' => 'col-sharedprice'

                       
                ]
        );
		 $this->addColumn(
            'newprice',
            [
                        'header' => __('New Price'),
                        'type' => 'currency',
						'renderer' =>'IWD\Pricesetup\Block\Adminhtml\Newprice',
						'index' => 'price'
                ]
        );
        return parent::_prepareColumns();
    }

    protected function getSelectedProducts()
    {
        $products = $this->getProductsPricesetup();
        
        return $products;
    }
     
    public function getGridUrl()
    {
        return $this->getUrl('*/*/productinfogrid', ['_current' => true]);
    }
}
