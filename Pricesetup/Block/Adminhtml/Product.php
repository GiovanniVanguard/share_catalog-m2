<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
  Support Email	: support@iwd.com 
 ***************************************************************************/
namespace IWD\Pricesetup\Block\Adminhtml;

class Product extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_product';
        $this->_blockGroup = 'IWD_Pricesetup';
        $this->_headerText = __('Product');
        $this->_addButtonLabel = __('Next');
        parent::_construct();
		 $this->addButton(
        'back',
        [
            'label' => __('Back'),
            'onclick' => "setLocation('" . $this->getUrl('sharedcatalogs/data/index') . "')",
            'class' => 'back'
        ],
        -1
    );

    }
}
