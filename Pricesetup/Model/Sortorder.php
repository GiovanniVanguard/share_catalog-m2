<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Copyright		: Copyright (c) 2018 iwd, http://www.iwd.com
 Support Email	: support@iwd.com 
 ***************************************************************************/
namespace IWD\Pricesetup\Model;

class Sortorder implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' =>'asc', 'label' => __('Ascending')],['value' =>'desc', 'label' => __('Descending')]];
    }

    public function toArray()
    {
        return [ 1 => __('Descending'),2=>__('Ascending')];
    }
}
