<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Copyright		: Copyright (c) 2018 iwd, http://www.iwd.com
 Support Email	: support@iwd.com 
 ***************************************************************************/
namespace IWD\Pricesetup\Model;

class Showproduct implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 2, 'label' => __('Display Category Wise')],['value' => 1, 'label' => __('Display All Products')]];
    }

    public function toArray()
    {
        return [1 => __('Display All Products'),2=>__('Display Category Wise')];
    }
}
