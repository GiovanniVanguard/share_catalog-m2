<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 

 ***************************************************************************/
namespace IWD\Pricesetup\Model\ResourceModel;

class Customcollection extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('iwd_pricesetup_products', 'id');
    }
}
