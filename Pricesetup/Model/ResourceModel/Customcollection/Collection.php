<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Copyright		: Copyright (c) 2018 iwd, http://www.iwd.com
 Support Email	: support@iwd.com 
 ***************************************************************************/
namespace IWD\Pricesetup\Model\ResourceModel\Customcollection;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('IWD\Pricesetup\Model\Customcollection', 'IWD\Pricesetup\Model\ResourceModel\Customcollection');
    }
}
