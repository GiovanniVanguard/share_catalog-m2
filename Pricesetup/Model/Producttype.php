<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Support Email	: support@iwd.com 
 ***************************************************************************/
namespace IWD\Pricesetup\Model;

class Producttype implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 2, 'label' => __('Auto')],['value' => 1, 'label' => __('Manually')], ['value' => 0, 'label' => __('Both')]];
    }

    public function toArray()
    {
        return [1 => __('Manually'),2=>__('Auto'),0 => __('Both')];
    }
}
