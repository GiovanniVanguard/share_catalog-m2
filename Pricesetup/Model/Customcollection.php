<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Support Email	: support@iwd.com 
 ***************************************************************************/
namespace IWD\Pricesetup\Model;

class Customcollection extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('IWD\Pricesetup\Model\ResourceModel\Customcollection');
    }
}
