<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Copyright		: Copyright (c) 2018 iwd, http://www.iwd.com
 Support Email	: support@iwd.com 
 ***************************************************************************/
namespace IWD\Pricesetup\Model;

class Sortby implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' =>'position', 'label' => __('Position')],['value' =>'price', 'label' => __('Price')], ['value' =>'name', 'label' => __('Name')]];
    }

    public function toArray()
    {
        return [0 => __('Name'), 1 => __('Price'),2=>__('Position')];
    }
}
