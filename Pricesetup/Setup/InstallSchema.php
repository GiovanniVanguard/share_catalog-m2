<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Copyright		: Copyright (c) 2018 iwd, http://www.iwd.com
 Support Email	: support@iwd.com 
 ***************************************************************************/
namespace IWD\Pricesetup\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

          $table = $installer->getConnection()->newTable(
              $installer->getTable('iwd_pricesetup_products')
          )->addColumn(
              'id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
              'id'
          )->addColumn(
              'entity_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'entity_id'
          )->addColumn(
              'store_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              11,
              ['unsigned' => true,'nullable' => false],
              'store_id'
          )->addColumn(
              'share_cat_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              11,
              ['unsigned' => true,'nullable' => false],
              'share_cat_id'
          )->addColumn(
              'offer',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              11,
              ['unsigned' => true,'nullable' => false],
              'offer'
          )->addColumn(
                'share_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                [],
                'Share Price'
            );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
