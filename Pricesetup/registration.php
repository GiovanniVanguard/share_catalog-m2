<?php
/***************************************************************************
 Extension Name	: Pricesetup Products 
 Support Email	: support@iwd.com 
 ***************************************************************************/
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'IWD_Pricesetup',
    __DIR__
);
